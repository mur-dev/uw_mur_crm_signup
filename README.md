# UW MUR CRM Signup
This module is the MUR CRM signup form.

[uwaterloo.ca/future-students/admin/config/undergraduate-recruitment-signup](https://uwaterloo.ca/future-students/admin/config/undergraduate-recruitment-signup)

---
---
# Functions

---
---
### function uw_mur_crm_signup_menu()
This hook enables the module to register paths in order to define how URL requests are handled.

##### Returns
An associative array whose keys define paths and whose values are an associative array of properties for each path.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
---
### function uw_mur_crm_signup_permissions($roles = array())
This gives custom access based on what the role permissions are.

##### Returns
If the user is an admin, it will return true and give them permission to view and edit the page. It will return false otherwise.

---
---
### function uw_mur_crm_signup_admin()
Allows for the configuration of this module by a site manager. These are the fields that can be modified in CKEditor in drupal.

##### Returns
The fields that are editable by a site manager.

---
---
### function uw_mur_crm_signup_form($form_state)
This is the actual form of the page that users will see. It is comprised of various types, including, but not limited to:
- button
- checkbox
- markup
- select
- submit
- textarea
- textfield

##### Returns
The form that is rendered and viewable by the user.

##### Additional Information
See the [form api documentation](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x) for the various types, their descriptions, their properties, and their examples.

---
---
### function uw_mur_crm_signup_validate($form, &$form_state)
Validates the input of the user. If there is an issue, the form will reset and an error will appear indicating the issue to the user.

---
---
### function uw_mur_crm_signup_submit($form, &$form_state)
Submits the form and sends the data to the CRM to be stored.

---
---
### function uw_mur_crm_signup_crm_authorization(&$session_id, &$url)
This authorizes access to the CRM.

---
---
### function uw_mur_crm_signup_crm_call($method, $parameters, $url)
This is the cURL request that transfer the data.

##### Returns
The response of the server.

---
---
### function uw_mur_crm_signup_add_lead_crm($first_name, $last_name, $address1, $address2, $city, $province, $country, $postal_code, $email, $phone_number, $citizenship, $start_year, $opt_out)
Saves the submitted data to the CRM. It adds a lead and stores data such as first name, last name, and etc.

##### Returns
The response of the server.